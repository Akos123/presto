<?php

return [
    'allAnnouncements'=>'Unsere produkte',
    'published'=>'Veröffentlicht auf :',
    'createHeader'=> 'Erstellen Sie Ihre Ankündigung',
    'indexAnnouncementHeader' => 'Alle Ankündigungen',
    'iTuoiDati'=>'Deine Daten',
    'visualizza'=> 'Zeigen',
    'categoria'=>'Kategorie :',
    'alertNoAnn'=> 'Für diese Kategorie sind keine Ankündigungen verfügbar',
    'announcement' => 'Titel der Ankündigung :',
    'title' => 'Titel:',
    'descrizione' => 'Beschreibung:',
    'prezzo' => 'Preis :€',
    'autore' => 'Autor: ',
    'accedi' => 'Einloggen',
    'accediCon' => 'Ansonsten melden Sie sich mit an',
    'password'  => 'Geben Sie hier Ihr Passwort ein',
    'nonSeiRegistrato' => 'Noch nicht registriert?',
    'register' => 'Registrieren',
    'nomePlaceholder' => 'Name',
    'passwordConfirmationPlaceholder' => 'Bestätigen Sie Ihr Passwort',
    'newsletterCeckbox' => 'Klicken Sie hier, um unseren Newsletter zu abonnieren',
    'registerWith' => 'Sonst melde dich bei:',
    'giaRegistrato' =>'Bereits registriert?',
    'giochiDelMomento' =>'Trendige Spiele',
    'vediTutto' => 'Alles sehen',
    'ilMigliorEcommerce' => 'Der beste Spiele-E-Commerce',
    'lavoraConNoi' => 'Möchten Sie mit uns zusammenarbeiten?',
    'registratiEclicca' => 'Click here',
    'diventaRevisore' => 'Revisor werden',
    'prodotti' => 'Produktes',
    'linkUtili' => 'Nützliche Links',
    'prezzi' => 'Preise',
    'impostazioni' => 'Einstellungen',
    'ordini' => 'Aufträge',
    'supporto' => 'Die Unterstützung',
    'contatti' => 'Kontakte',
    'compraOra' => 'Kaufe jetzt', 
    'annunci' => 'Ankündigungen',
    'categorie' => 'Kategorien', 
    'suDiNoi' => 'Über uns', 
    'benvenuto' => 'Herzlich willkommen',
    'zonaRevisore' => 'Revisor-Sektion',
    'messaggiNonLetti' => 'Ungelesene Nachrichten', 
    'esci' =>'Ausloggen',
    'profilo' => 'Mein Profil',
    'affidabileSicuro' => 'Sicher und vertrauenswürdig',
    'servizioClienti' => 'Kundendienst',
    'velocita' => 'Expresszustellung',
    'scegliCategoria' => 'Kategorie wählen',
    'anteprimaFoto' => 'Fotovorschau',
     'cancella' => 'Löschen',
     'creaAnnuncio' => 'Ankündigung erstellen',
     'immaginePlaceholder' => 'Bild',
     'revisorPage' => 'Revisor-Sektion',
     'annunciSi' => 'Hier sind die Ankündigungen zu überprüfen',
     'annunciNo' => 'Es gibt keine Ankündigungen zu prüfen',
     'da' => 'durch :',
     'accetta' =>   'Annehmen',
     'rifiuta' => 'Sich weigern',
     'picchiaduro' => 'Kampfspiel',
     'corse' => 'Wettrennen',
     'carte' => 'Karte',
     'azione' => 'Aktion',
     'esploraCategoria' => 'Entdecken Sie die Kategorie:',
     'categoryNo' => 'Keine Ankündigungen für diese Kategorie',
     'publicaneUno' => 'Veröffentlichen Sie eine  :', 
     'welcomeBlade' => 'Willkommen im Lucky Game',
     'proponiAnnuncio' => 'Veröffentlichen Sie eine Ankündigung',
     'categoriaHome' => 'Kategorien',
     'teamHeader' => 'Unser Team',
     'profilo' => 'Profil',
];