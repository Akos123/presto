<?php

namespace App\Http\Livewire;

use File;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;

class CreateAnnouncement extends Component
{   
    use WithFileUploads;
   
   public $title;
   public $body;
   public $price;
   public $category;
   public $temporary_images;
   public $images = [];
   public $announcement;
   public $validated;

   
   

   protected $rules = [

    'title'=>'required|min:4',
    'body'=>'required|min:8',
    'price'=>'required|numeric',
    'category'=>'required',
    'images.*'=>'image|max:2048',
    'temporary_images.*'=>'image|max:2048',
    
   ];

   protected $message= [

    'required'=>'Il campo :attribute deve essere inserito ',
    'min'=>'Il campo :attribute deve contenere altri caratteri',
    'numeric'=>'Il campo :attribute dev\`essere un numero',
    'temporary_images.required'=>'Immagine è richiesta',
    'temporary_images.*.image' =>'I file devono essere immagini',
    'temporary_images.*.max' =>'Grandezza immagine di massimo 2mb',
   
   ];

   public function updatedTemporaryImages(){

        if ($this->validate([
            'temporary_images.*'=>'image|max:2048',
            ])) {
            foreach ($this->temporary_images as $image){
                    $this->images[] = $image;
            }
        }
   }

   public function removeImage($key){

        if (in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
   }
   
   public function store()
   {    

        $this->validate();

        $this->announcement = Category::find($this->category)->announcements()->create($this->validate());
        if(count($this->images)){
            foreach ($this->images as $image) {
               
                $newFileName = "announcements/{$this->announcement->id}";
                $newImage = $this->announcement->images()->create(['path'=>$image->store($newFileName , 'public')]);

                RemoveFaces::withChain([
                    
                    new ResizeImage($newImage->path , 400 , 300),

                    new GoogleVisionSafeSearch($newImage->id),
                    
                    new GoogleVisionLabelImage($newImage->id)
                ])->dispatch($newImage->id);

            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }

        
                    // commentato perchè non so se possa servire
        // Auth::user()->announcements()->save($announcement);


        session()->flash('message','Annuncio inserito con successo');
        $this->cleanForm();

   }


   public function updated ($propertyName){

    $this->validateOnly($propertyName);

   }
   
   public function cleanForm(){

    $this->title = '';
    $this->body = '';
    $this->price = '';
    $this->category = '';
    $this->images = [];
    $this->temporary_images = [];


   }
    public function render()
    {
        return view('livewire.create-announcement');
    }
}
