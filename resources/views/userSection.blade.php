<x-layout>
  
  
    <x-slot name="header">{{__('ui.benvenuto')}}, {{Auth::user()->name}}</x-slot>


<div class="container ">
    <div class="row mx-1"> 
      <div class="col-12  shadowProfile my-2 d-flex  flex-column justify-content-center  ">

        <div class="card cardProfile shadow" >
          <div class="card-body">
            <h2 class="font-2 noi fw-bold text-uppercase text-light profileTextShadow" >{{__('ui.iTuoiDati')}}</h2>
            <h5 class="font-1 profileTextShadow text-light "> {{__('ui.nomePlaceholder')}} : {{Auth::user()->name}}</h5>
            <h5 class="font-1  pb-5 profileTextShadow text-light">Email : {{Auth::user()->email}}</h5>
           
            <a href="{{route('announcements.create')}}" class="font-1 nounderline  pb-5 profileTextShadow text-light">{{__('ui.proponiAnnuncio')}}</a>
           
          </div>
        </div>

      </div>
    </div>
  </div>
</x-layout>