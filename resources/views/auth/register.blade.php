<x-layout>

    <x-slot name="header">{{__('ui.register')}}</x-slot>

    <div class="container my-5 ">
        <form class="form-registration " action="{{route('register')}}" method="POST">
            @csrf
            <div class="row justify-content-center"> 
                  @if ($errors->any())
                    <div class="alert alert-danger">
                     <ul>
                       @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                       @endforeach
                     </ul>
                    </div>
                  @endif
                    <div class="col-12 col-md-5">
                        <div class="mb-3">
                            <label for="name" class="form-label "></label>
                            <input type="text" class="form-control bg-form-register2" required='true' id="name" placeholder="{{__('ui.nomePlaceholder')}}" name="name">
                        </div>
                    </div>
                    
                    <div class="col-12 col-md-5">
                        <div class="mb-3 x">
                            <label for="email" class="form-label "></label>
                            <input type="email" class="form-control bg-form-register2" placeholder="email" required='true' id="email" name="email">
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="mb-3 ">
                            <label for="password" class="form-label"></label>
                            <input type="password" class="form-control bg-form-register2" placeholder="password" required='true' id="password" name="password">
                        </div>
                    </div>
                       
                    <div class="col-12 col-md-5">
                        <div class="mb-3 ">
                            <label for="password_confirmation" class="form-label"></label>
                            <input type="password" class="form-control bg-form-register2" placeholder="{{__('ui.passwordConfirmationPlaceholder')}}" required='true' id="password_confirmation" name="password_confirmation">
                        </div>
                    </div>
            
                <div class="col-12 col-md-3">
                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="">
                        <label class="text-light" for="exampleCheck1">{{__('ui.newsletterCeckbox')}}</label>
                      </div>
                </div>
               
                  
                  <div class="d-flex justify-content-center mb-3">
                    <button type="submit" class="btn btn-primary rounded">{{__('ui.register')}}</button>
                  </div>
               <div>
                <p class="my-3 d-flex justify-content-center text-light ">{{__('ui.registerWith')}}</p>
                <div class="d-flex justify-content-center"> 
                    
                 <a href="https://www.facebook.com/">  <i class="fab fa-facebook card-font-register fa-2x mb-4"></i></a>
                 <a href="https://www.twitter.com/"><i class="fab fa-twitter card-font-register fa-2x mb-4"></i></a>
                 <a href="https://www.instagram.com/"><i class="fab fa-google card-font-register fa-2x mb-4"></i></a>
                </div>
                <div class=" d-flex justify-content-center">
                    <a href="{{route('login')}}">{{__('ui.giaRegistrato')}}</a>
                  </div>
               </div>
                  
                       
                    </form>    
        
            
              
                
                    
                
              
         </div>
    </div>

   
</x-layout>