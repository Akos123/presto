<x-layout>
    <x-slot name="header">{{__('ui.accedi')}}</x-slot>
    <div class="container my-5 register-container">
    
        <form class="form-registration" action="{{route('login')}}" method="post">
            @csrf
            <div class="row justify-content-center"> 
                @if ($errors->any())
                <div class="alert alert-danger">
                 <ul>
                   @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                   @endforeach
                 </ul>
                </div>
              @endif
                <div class="col-12 d-flex justify-content-center me-5 pe-5">
                    <div class="mb-3 pe-3 me-5">
                        <label for="email" class="form-label"></label>
                        <input type="email" class="form-control  bg-form" placeholder="Email" required='true' id="email" name="email">
                      </div>
                </div>
                <div class="col-12 d-flex justify-content-center me-5 pe-5">
                    <div class="mb-3 me-5 pe-3">
                        <label for="password" class="form-label"></label>
                        <input type="password" class="form-control bg-form" placeholder="{{__('ui.password')}}"required='true' id="password" name="password">
                      </div>
                </div>

                {{-- CHECKBOX LAVORARCI IN SEGUITO --}}
                {{-- <div class="col-12  ms-5 ps-5 media-checkbox">
                    <div class="mb-3 form-check">
                        <input type="checkbox"  class="form-check-input" id="">
                        <label class="form-check-label" name="remember" for="exampleCheck1">Spunta per mantenere l'accesso</label>
                      </div>
                </div> --}}
          
                
                <div class="d-flex justify-content-center">
                   
                    <button type="submit" class="btn btn-primary rounded">{{__('ui.accedi')}}</button>
            
                </div>
                <p class="my-3 d-flex justify-content-center text-light">{{__('ui.accediCon')}}</p>
                <div class="d-flex justify-content-center"> 
                    
                    <i class="fab fa-facebook card-font-register fa-2x mb-4"></i>
                    <i class="fab fa-twitter card-font-register fa-2x mb-4"></i>
                    <i class="fab fa-google card-font-register fa-2x mb-4"></i>
                </div>
                <div class=" d-flex justify-content-center">
                  <a href="{{route('register')}}">{{__('ui.nonSeiRegistrato')}}</a>
                </div>
              </form>
         </div>
    </div>

   
</x-layout>