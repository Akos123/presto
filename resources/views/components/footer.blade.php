<footer class="text-center text-lg-start bg-colorB text-white  "> 

      
  <!-- Section: Social media -->
  <div class="container-fluid justify-content-center d-flex p-4 pb-0 bg-color5">
    <!-- Section: Social media -->
    
    <!-- Twitter -->
    <a  href="https://twitter.com"><i class="fab fa-twitter card-font fa-2x mb-4"></i></a>
    
    {{-- <!-- Google -->
    <i class="fab fa-google card-font fa-2x mb-4" href="https://www.google.com"></i> --}}
    
    <!-- Instagram -->
    <a  href="https://instagram.com"><i class="fab fa-instagram card-font fa-2x mb-4 mx-2" ></i></a>
    
    <!-- Linkedin -->
    <a  href='https://linkedin.com'><i class="fab fa-linkedin-in card-font  fa-2x mb-4" ></i></a>
    
    {{-- <!-- Youtube -->
    <i class="fab fa-youtube card-font fa-2x  mb-4" href="https://www.youtube.com"></i>
     --}}
    <!-- Github -->
    <a  href="https://github.com"><i class="fab fa-github card-font  fa-2x mb-4 mx-2" ></i></a>
    
    {{-- <!-- Whatsapp -->
    <i class="fab fa-whatsapp card-font fa-2x   mb-4" href="https://www.whatsapp.com"></i> --}}
    <!-- Section: Social media -->
  </div>

  


  <!-- Section: Links  -->
  <section class=" bg-colorB">
    <div class="container text-center text-md-start mt-5 ">
      <!-- Grid row -->
      <div class="row mt-3">
        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <h6 class="text-uppercase fw-bold mb-4 letterspacing">
            <i class="fas fa-gem me-3 "></i><strong>
              Lucky Game
            </strong>
          </h6>
          <p class="font-2">
            {{__('ui.ilMigliorEcommerce')}}
          </p>
          {{-- pulsante peppuzzo --}}

              
          @if(Auth::check() && Auth::user()->is_revisor)

          <a href="#" class="text-color3 display-6">Presto.it</a>
          @else
                <a href="#" class="text-color3 fw-bold display-6">Presto.it</a>
                <p class="font-2">{{__('ui.lavoraConNoi')}}</p>
                <p class="font-2">{{__('ui.registratiEclicca')}}</p>
                <a href="{{route('become.revisor')}}" class="btn btn-warning card-font-btn-footer ms-1">{{__('ui.diventaRevisore')}}</a>
          @endif
             
        </div>
        <!-- Grid column -->
        
        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4 nocell">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            {{__('ui.prodotti')}}
          </h6>
          <p>
            <a href="#!" class="text-reset text-decoration">HTML</a>
          </p>
          <p>
            <a href="#!" class="text-reset text-decoration">CSS</a>
          </p>
          <p>
            <a href="#!" class="text-reset text-decoration">BOOTSTRAP</a>
          </p>
          <p>
            <a href="#!" class="text-reset text-decoration">Laravel</a>
          </p>
        </div>
        <!-- Grid column -->
        
        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4 nocell ">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            {{__('ui.linkUtili')}}
          </h6>
          <p>
            <a href="#!" class="text-reset text-decoration">{{__('ui.prezzi')}}</a>
          </p>
          <p>
            <a href="#!" class="text-reset text-decoration">{{__('ui.impostazioni')}}</a>
          </p>
          <p>
            <a href="#!" class="text-reset text-decoration">{{__('ui.ordini')}}</a>
          </p>
          <p>
            <a href="#!" class="text-reset text-decoration">{{__('ui.supporto')}}</a>
          </p>
        </div>
        <!-- Grid column -->
        
        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            {{__('ui.contatti')}}
          </h6>
          <p><i class="fas fa-home me-3"></i> New York, NY 10012, US</p>
          <p>
            <i class="fas fa-envelope me-3"></i>
            info@example.com
          </p>
          <p><i class="fas fa-phone me-3"></i> + 01 234 567 88</p>
          <p><i class="fas fa-print me-3"></i> + 01 234 567 89</p>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
  </section>
  <!-- Section: Links  -->
  
  
  
  <!-- Footer -->
  
  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2020 Copyright:
    <a class="text-white text-decoration" href="https://mdbootstrap.com/">Presto</a>
  </div>
  <!-- Copyright -->
</footer>