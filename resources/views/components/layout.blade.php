<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lucky game</title>
    
    <script src="https://kit.fontawesome.com/1b732a08c8.js" crossorigin="anonymous"></script>

    @livewireStyles
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body class=" bg-color1">
    <x-navbar />

    <x-header header={{$header}} />
   

    {{$slot}}

    <x-footer />
    @livewireScripts
    
</body>
</html>