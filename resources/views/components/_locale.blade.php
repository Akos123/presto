<form method="POST" action="{{route('set_language_locale', $lang)}}" >
    @csrf
    <button class="nav-link bg-transparent border-0" type="submit" >
        <span class="flag-icon flag-icon-{{$nation}}"></span>
    </button>
</form>