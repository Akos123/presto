<div class="container-fluid bg-color5 py-1 ">
    <div class="container">
      <div class="row ">
        <div class="col-12 d-flex justify-content-evenly py-4 d-none d-md-flex ">
          <i class="fa-solid fa-shield-halved fa-2x font"></i>
          <h5 class=" fw-bold align-items-center text-color3  ">{{__('ui.affidabileSicuro')}}</h5>
          <div class="linea"></div>
          <i class="fa-solid fa-comments fa-2x font"></i>
          <h5 class=" fw-bold  align-items-center  text-color3  ">{{__('ui.servizioClienti')}}</h5>
          <div class="linea"></div>
          <i class="fa-solid fa-truck-fast fa-2x font"></i>
          <h5 class=" fw-bold align-items-center text-color3   ">{{__('ui.velocita')}}</h5>
        </div>
        <!-- cellullare -->
        <div class="col-12 d-flex justify-content-evenly py-4 d-flex d-md-none ">
          <i class="fa-solid fa-shield-halved fa-2x font"></i>
          <div class="linea"></div>
          <i class="fa-solid fa-comments fa-2x font"></i>
          <div class="linea"></div>
          <i class="fa-solid fa-truck-fast fa-2x font"></i>
        </div>
      </div>
    </div>
  </div>