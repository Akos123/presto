<section class="container  vh-10  ">
    <div class="row ">
      <div class="col-12 d-flex justify-content-between py-5 pe-4   ">
        <h2 class="text-color1 ">{{__('ui.giochiDelMomento')}}</h2>
        <!-- da pensare forse metto la freccia che va per giu -->
        {{-- <a class=" text-color3  text-uppercase py-1 px-2 cursor">{{__('ui.vediTutto')}}</a> --}}
      </div>
    </div>
  </section>

   <!-- card -->
   <div class="container">
    <div class="row pt-5 ">
      <!-- card1 -->
      <div class="col-4   ">
        <div class="bg-card card1">
        </div>
      </div>
      <!-- card2 -->
      <div class="col-4 ">
        <div class="bg-card card2">
        </div>
      </div>
      <!-- card3 -->
      <div class="col-4  ">
        <div class="bg-card card3">
        </div>
      </div>
      <!-- sotto -->
      <!-- card4 -->
      <div class="col-4  pt-5 pb-5 ">
        <div class="bg-card card4">
        </div>
      </div>
      <!-- card5 -->
      <div class="col-4 pt-5 pb-5">
        <div class="bg-card card5">
        </div>
      </div>
      <div class="col-4  pt-5 pb-5">
        <div class="bg-card card6">
        </div>
      </div>
      
      
    </div>
  </div>