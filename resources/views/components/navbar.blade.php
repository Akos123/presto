<nav id="mainNavbar" class="navbar navbar-expand-lg fixed-top p-0  bg-color7">
    <a href="{{route('welcome')}}" class="navbar-brand p-0  {{(Route::currentRouteName() == 'welcome') ? 'active' : ''}}">
        <h1 class="ms-2 titolo fw-bold pt-1" >Presto.it</h1>
        <!-- <img src="img/logo.png" alt="Logo"> -->
    </a>
    <button class="navbar-toggler bg-color3 me-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span class="fa fa-bars "></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ms-auto py-0">
            <a href="{{route('announcement.index')}}" class="nav-item nav-link  {{(Route::currentRouteName() == 'announcement.index') ? 'active' : ''}} text-color1">{{__('ui.annunci')}}</a>
            
            {{-- DROPDOWN CATEGORIE --}}
            <li class="nav-item dropdown text-color1 ">
                <a href="#" id="categoriesDropdown" class="nav-link  dropdown-toggle text-color1" role="button" data-bs-toggle="dropdown" aria-expanded="false" >
                    {{__('ui.categorie')}}
                </a>
                <ul class="dropdown-menu bg-color1" aria-labelledby="categoriesDropdown">
                    <li><a class="dropdown-item text-color1" href="{{route('categorie')}}">{{__('ui.vediTutto')}}</a></li>
                    @foreach ($categories as $category)
                   
                    <li> <a class="dropdown-item text-color4" href="{{route('categoryShow', compact('category'))}}">
                        
                        {{($category->name)}}</a></li>
                        
                        @endforeach       
                         
                    </ul>
                </li>
                {{-- FINE DROPDOWN CATEGORIE --}}
                <a href="{{route('aboutUs')}}" class="nav-item nav-link  text-color1">{{__('ui.suDiNoi')}}</a>
                {{-- <a href="service.html" class="nav-item nav-link  text-color1">Service</a> --}}
                
                @guest
                <div class="nav-item dropdown ">
                    <a href="#" class="nav-item nav-link dropdown-toggle text-color1   me-3" data-bs-toggle="dropdown">{{__('ui.benvenuto')}} , guest</a>
                    <div class="dropdown-menu m-0 bg-color1 ">
                        <a href="{{route('register')}}" class="dropdown-item  text-color4">{{__('ui.register')}}</a>
                        <a href="{{route('login')}}" class="dropdown-item  text-color4">{{__('ui.accedi')}}</a>
                        
                        
                    </div>
                </div>
                @else
                <div class="nav-item dropdown ">
                    <a href="#" class=" nav-link dropdown-toggle text-color1 " 
                    data-bs-toggle="dropdown">{{__('ui.benvenuto')}}, {{Auth::user()->name}}</a>
                    
                    @if (Auth::user()->is_revisor)
                    <div class="dropdown-menu m-0 bg-color1 ">
                        <a class="dropdown-item text-color4" href="{{route('revisor.index')}}" >{{__('ui.zonaRevisore')}}
                            <span> {{App\Models\Announcement::toBeRevisionedCount()}}
                                <span class="visually-hidden">{{__('ui.messaggiNonLetti')}}</span>
                            </span>
                        </a>
                        <a href="{{route('userSection')}}" class="dropdown-item  text-color4">{{__('ui.profilo')}}</a>
                        <li><a class="dropdown-item text-color4" href="/logout" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">{{__('ui.esci')}}</a></li>
                    </div>
                    @endif
                    <div class="dropdown-menu m-0 bg-color1 ">
                        <a href="{{route('userSection')}}" class="dropdown-item  text-color4">{{__('ui.profilo')}}</a>
                        
                        <li><a class="dropdown-item text-color4" href="/logout" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">{{__('ui.esci')}}</a></li>
                        <form method="POST" id="form-logout" action="{{route('logout')}}" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
                @endguest
                {{-- bottone selezione lingua, perche' non c'e' ul ? --}}
                <div class="dropdown mx-2">
                    <button class="btn dropdown-toggle bg-warning " type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa-solid fa-globe" aria-hidden="true"></i>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li class="nav-item dropdown-item d-flex align-items-center">
                           <x-_locale lang="it" nation='it'/>
                           IT
                        </li>
                        <li class="nav-item dropdown-item d-flex align-items-center">
                            <x-_locale lang="en" nation='gb'/>
                            EN
                        </li>
                        <li class="nav-item dropdown-item d-flex align-items-center">
                            <x-_locale lang="de" nation='de'/>
                            DE
                        </li>
                    </ul>
                  </div>
                
                <div class="d-flex altezza">
                    <form action='{{route('announcements.search')}}' method='GET' class="d-flex  " >
                        <input name='searched' class='form-control py-0 ' type="search"  placeholder='search'>
                        <button class='card-font3 d-flex align-items-center rounded' type='submit'><i class="fa-solid fa-magnifying-glass text-light  text-color3 "></i></button>   
                    </form>
                </div>
            </div>
        </nav>