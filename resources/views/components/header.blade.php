   <!-- header -->
   <header class="container-fluid  ">
    <div class="row  bg-header    ">
      <div class="col-12  pt-5 mt-2 cellulare   ">
        <!-- media querry sinistra 2 -->
        <h1 class="fw-bold  text-uppercase justify-content-center cellulare    align-items-center d-flex pt-5  text-color5 0">{{$header}} </h1>
        <div class="col-12  d-flex justify-content-center pt-4  ">
          <a href="#" >
            <button  class=" btn-custom1 px-5 fw-bold py-2 text-uppercase text-color2 ">{{__('ui.compraOra')}}</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</header>