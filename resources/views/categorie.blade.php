<x-layout>

    <x-slot name="header">{{__('ui.categorie')}}</x-slot>

    <main class="bg-color1">
        <div class="container  ">
            <!-- card over900px -->
            <div class="row d-none d-md-flex ">
                <div class="col-12 d-flex justify-content-between pb-5 pt-5 ">
                    <h2 class=" text-color3 fw-bold">{{__('ui.categorie')}}</h2>  
                </div>
                <!-- card -->
                <div class="col-4   ">
                    <a href="{{route('categoryShow', $category[0])}}"  class="nounderline">
                        <div class="bg-card2 bg-img d-flex justify-content-between">             
                            <h4 class="align-items-center d-flex text-white   ms-3"> Arcade</h4>
                            <img class="img-fluid  transform1 " src="./media/category-icon2.webp">
                        </div>
                    </a>
                </div>
                <!-- card2 -->
                <div class="col-4   ">
                    <a href="{{route('categoryShow',$category[1])}}"  class="nounderline">
                        <div class="bg-card2 bg-img2 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white   ms-1">{{__('ui.picchiaduro')}}</h4>
                            <img class="img-fluid  pe-3 transform1 " src="./media/tekken.webp">
                        </div>
                    </a>
                </div>
                <!-- card3 -->
                <div class="col-4   ">
                    <a  href="{{route('categoryShow', $category[2])}}" class="nounderline" >
                        <div class="bg-card2 bg-img3 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white   ms-3">Indie</h4>
                            <img class="img-fluid  transform2  " src="./media/the-devil-680032-removebg-preview (2).png">
                        </div>
                    </a>
                </div>
                <!-- sezione 2  -->
                <!-- card1 -->
                <div class="col-4 pt-5  ">
                    <a href="{{route('categoryShow', $category[3])}}" class="nounderline">
                        <div class="bg-card2 bg-img4 d-flex justify-content-between">
                            
                            <h4 class="align-items-center d-flex text-white   ms-3">FPS</h4>
                            <img class="img-fluid  transform3 " src="./media/Overwatch-Character-PNG-Image-File.png">
                        </div>
                    </a>
                </div>
                <!-- card2 -->
                <div class="col-4  pt-5 ">
                    <a class="nounderline"  href="{{route('categoryShow', $category[4])}}">
                        <div class="bg-card2 bg-img5 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white    ms-3">{{__('ui.corse')}}</h4>
                            <img class="img-fluid  transform1 me-3 " src="./media/2.png">
                        </div>
                    </a>
                </div>
                <!-- card3 -->
                <div class="col-4 pt-5  ">
                    <a class="nounderline" href="{{route('categoryShow', $category[5])}}">
                        <div class="bg-card2 bg-img6 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white   ms-2  ">Souls-Like</h4>
                            <img class="img-fluid  transform3  " src="./media/28-289008_dark-souls-artorias-png-transparent-png-removebg-preview (1).png">
                        </div>
                    </a>
                </div>
                <!-- sezion3 -->
                <!-- card1 -->
                <div class="col-4 pt-5  ">
                    <a href="{{route('categoryShow', $category[6])}}" class="nounderline">
                        <div class="bg-card2 bg-img7 d-flex justify-content-between">
                            
                            <h4 class="align-items-center d-flex text-white   ms-2">{{__('ui.azione')}}</h4>
                            <img class="img-fluid  transform4 pe-4 " src="./media/64d081ae340acd5b38c33ba2d1ec12d3-removebg-preview.png">
                        </div>
                    </a>
                </div>
                <!-- card2 -->
                <div class="col-4  pt-5 ">
                    <a class="nounderline" href="{{route('categoryShow', $category[7])}}">
                        <div class="bg-card2 bg-img8 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white   ms-3">{{__('ui.carte')}}</h4>
                            <img class="img-fluid  transform5 " src="./media/png-clipart-yu-gi-oh-yugi-mutou-yami-yugi-t-shirt-yu-gi-oh-duel-links-seto-kaiba-magician-poster-fictional-character-removebg-preview.png">
                        </div>
                    </a>
                </div>
                <!-- card3 -->
                <div class="col-4 pt-5  ">
                    <a class="nounderline" href="{{route('categoryShow', $category[8])}}">
                        <div class="bg-card2 bg-img9 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white  ps-2   text-decoration ">RPG</h4>
                            <img class="img-fluid  transform5 p   " src="./media/fallout-new-vegas-fallout-3-fallout-4-vault-tec-workshop-the-vault-video-game-vault-boy-removebg-preview.png">
                        </div>
                    </a>
                </div>
                <!-- sezione 4 -->
                <div class="col-4 pt-5  pb-5 ">
                    <a href="{{route('categoryShow', $category[9])}}" class="nounderline">
                        <div class="bg-card2 bg-img10 d-flex justify-content-between">
                            
                            <h4 class="align-items-center d-flex text-white   ms-3">Sandbox</h4>
                            <img class="img-fluid  transform5   " src="./media/m2i8i8N4H7i8G6d3-removebg-preview.png">
                        </div>
                    </a>
                </div>
                <!-- card2 -->
                <div class="col-4  pt-5 ">
                    <a class="nounderline"  href="{{route('categoryShow', $category[10])}}" >
                        <div class="bg-card2 bg-img11 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white   ms-3">Horror</h4>
                            <img class="img-fluid  pe-5 transform2  " src="./media/10fd60d120ecfb163eaf6be0c4fcf93a-removebg-preview.png">
                        </div>
                    </a>
                </div>
                <!-- card3 -->
                <div class="col-4 pt-5  ">
                    <a class="nounderline"  href="{{route('categoryShow', $category[11])}}" >
                        <div class="bg-card2 bg-img12 d-flex justify-content-between">
                            <h4 class="align-items-center d-flex text-white   ms-3 text-decoration ">Moba</h4>
                            <img class="img-fluid  pe-5 transform6  " src="./media/02_RK_Concept_final_Web-removebg-preview.png">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        
        <div class="container  ">
            <!-- card over900px -->
            <div class="row d-flex d-md-none ">
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center   ">
                        <a class="text-decoration text-center fw-bold text-uppercase "href="#">Arcade</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center   ">
                        <a class="text-decoration text-center fw-bold text-uppercase"href="#" >{{__('ui.picchiaduro')}}</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900  d-flex justify-content-center ">
                        <a class="text-decoration text-center fw-bold text-uppercase " href="#">Indie</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center  ">
                        <a class="text-decoration text-center fw-bold text-uppercase "href="#">FPS</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center   ">
                        <a class="text-decoration text-center fw-bold text-uppercase"href="#" >{{__('ui.corse')}}</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900  d-flex justify-content-center ">
                        <a class="text-decoration text-center fw-bold text-uppercase " href="#">Souls-Like</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center  ">
                        <a class="text-decoration text-center fw-bold text-uppercase "href="#">{{__('ui.azione')}}</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center   ">
                        <a class="text-decoration text-center fw-bold text-uppercase"href="#" >{{__('ui.carte')}}</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900  d-flex justify-content-center ">
                        <a class="text-decoration text-center fw-bold text-uppercase " href="#">RPG</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center  ">
                        <a class="text-decoration text-center fw-bold text-uppercase "href="#">Sandbox</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900 d-flex justify-content-center   ">
                        <a class="text-decoration text-center fw-bold text-uppercase"href="#" >Horror</a>
                    </div>
                </div>
                <div class="col-4 pt-5">
                    <div class="card-900  d-flex justify-content-center ">
                        <a class="text-decoration text-center fw-bold text-uppercase " href="#">Moba</a>
                    </div>
                </div>
            </div>
        </div>
        
        
        
    </main>
</x-layout>