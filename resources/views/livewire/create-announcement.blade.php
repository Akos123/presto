<div>  
    @if (session('message'))
    <div class=" flex flex-row justify-content-center my-2 alert alert-success">
        {{session('message')}}
    </div>
    @endif     
    <div class="container pe-5  d-flex justify-content-center ">
        <div class="row my-4 pe-5 me-5  ">
            <div class="col-12 pe-5">
               
                <form wire:submit.prevent="store"> 
                    @csrf
                    {{-- titolo annuncio form creazione --}}
                    <div class="mb-3   ">
                        <label for="title" class="form-label  font-2   text-center ">{{__('ui.title')}}</label>
                        <input wire:model.lazy="title" type="text" placeholder="{{__('ui.announcement')}}" class="form-control bg-form font-2 @error('title') is-invalid @enderror">
                        @error('title')
                        <span class="fst-italic text-danger small">{{$message}}</span>
                        @enderror
                    </div>
                    {{-- descrizione annuncio form creazione --}}
                    <div class="mb-3">
                        <label for="body" class="form-label  font-2" >{{__('ui.descrizione')}}</label>
                        <textarea wire:model.lazy="body" id="body" cols="30" rows="10" placeholder="{{__('ui.descrizione')}}" class="form-control bg-form @error('body') is-invalid @enderror"></textarea>
                        @error('body')
                        <span class="fst-italic text-danger small">{{$message}}</span>
                        @enderror
                    </div>
                    
                    {{-- <div class="col-12 col-md-5">
                        <div class="mb-3">
                            <label for="formFile" class="form-label">Inserisci un immagine di segnaposto</label>
                            <input class="form-control" type="file"  id="formFile">
                        </div>
                    </div> --}}
                    {{-- prezzo annuncio form creazione --}}
                    <div class="mb-3">
                        <label for="price" class="form-label font-2 " >{{__('ui.prezzo')}}</label>
                        <input wire:model='price' type="number" class='form-control bg-form @error('price') is-invalid @enderror'>
                        @error('price')
                        <span class="fst-italic text-danger small">{{$message}}</span>
                        @enderror
                        
                    </div>
                    
                    {{-- categoria annuncio form creazione --}}
                    <div class="mb-3">
                        
                        <label for="category" class="form-label font-2 " >{{__('ui.categoria')}}</label>
                        <select wire:model.defer="category" id="category" class="form-control bg-form">
                            <option value="">{{__('ui.scegliCategoria')}}</option>
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- scelta immagini --}}
                    <div class="mb-3">
                        <input wire:model="temporary_images" type="file" name="images" multiple class="form-control bg-form @error('temporary_images.*') is-invalid @enderror" placeholder="{{__('ui.immaginePlaceholder')}}"/>
                        @error('temporary_images.*')
                            <p class="text-danger mt-2">{{$message}}</p>
                        @enderror
                    </div>
                    @if (!empty($images))
                        <div class="row">
                            <div class="col-12">
                                <p>{{__('ui.anteprimaFoto')}}</p>
                                <div class="row border border-4 border-info rounded shadow py-4">
                                    @foreach ($images as $key=>$image)
                                        <div class="col my-3">
                                            <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                                            <button type="button" class="btn btn-danger shadow d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">{{__('ui.cancella')}}</button>
                                        </div>
                                    @endforeach    
                                </div>
                            </div>
                        </div>
                    @endif    
                    <button type="submit" class="card-font2 ">{{__('ui.creaAnnuncio')}}</button> 
                </form>
            </div>
        </div>   
    </div>   
</div>


