<x-layout>
    <x-slot name="header">{{__('ui.esploraCategoria')}} {{$category->name}}</x-slot>
    <div class="container">

        <div class="row">
        <div class="col-12">
            <div class="row">
            @forelse ($category->announcements as $announcement)
            <div class="col-12 col-md-4  my-4 pt-5 d-flex ">
              <div class=" cardR  "  >             
                              
                  <img class="img-fluid ridimensionamento " src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400 , 300) : 'https://picsum.photos/500/500'}}">
                  <div class="info">
                    <h4 class="font-1 pt-3 text-center font-1 text-uppercase text-color5 fw-bold mt-5 ">{{$announcement->title}}</h5>
                    <p class="text-center font-1  text-color5 fw-bold fw-bold display-6">{{$announcement->price}}€</p>
                    <div class=" d-flex justify-content-around pt-4   ">
                      <a href="{{route('announcements.show' ,compact('announcement'))}}" class="  ms-2 text-color4 fw-bold btn-custom5 ">{{__('ui.visualizza')}}</a>
                      <a href="" class=" btn-custom4 me-2 text-color4 fw-bold  "> {{__('ui.categoria')}}{{$announcement->category->name}}</a>
                    </div>
                    <p class="  text-center pe-4 pt-3 text-color5 fw-bold mt-4 ">{{__('ui.published')}}{{$announcement->created_at->format('d/m/Y')}}</p>
                  </div>
                
              </div>
            </div>
            @empty
                    <div class="col-12">
                        <p class="h1 text-color1">{{__('ui.categoryNo')}}</p>
                        <p class="h2 text-color1">{{__('ui.publicaneUno')}} <a href="{{route('announcements.create')}}" class="btn btn-success shadow">Nuovo Annuncio</a></p>
                    </div>
            @endforelse
            </div>
        </div>
        </div>
    </div>

</x-layout>    