<x-layout>
  
  
  <x-slot name="header">{{__('ui.welcomeBlade')}}</x-slot>
  
  @if(session('access.denied'))
  
  <div class="alert alert-danger">
    <p>{{session('access.denied')}}</p>
  </div>
  @endif

  @if(session('message'))
  
  <div class="alert alert-success">
    <p>{{session('message')}}</p>
  </div>
  @endif
  
  
  <div class="container">   
    <div class="row ">
      <div class="col-12    ">
        <h2 class="h2 my-2 fw-bold text-color1 text-center ">{{__('ui.allAnnouncements')}}</h2>
      </div>
          @foreach ($announcements as $announcement)
          <div class="col-12 col-md-4  my-4 pt-5 d-flex ">
            <div class=" cardR  "  >             
                            
                <img class="img-fluid ridimensionamento " src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400 , 300) : 'https://picsum.photos/500/500'}}">
                <div class="info">
                  <h4 class="font-1 pt-3 text-center font-1 text-uppercase text-color5 fw-bold mt-5 ">{{$announcement->title}}</h5>
                  <p class="text-center font-1  text-color5 fw-bold fw-bold display-6">{{$announcement->price}}€</p>
                  <div class=" d-flex justify-content-around pt-2   ">
                    <a href="{{route('announcements.show' ,compact('announcement'))}}" class="  ms-2 text-color4 fw-bold btn-custom5 ">{{__('ui.visualizza')}}</a>
                    <a href="" class=" btn-custom4 me-2 text-color4 fw-bold  "> {{$announcement->category->name}}</a>
                  </div>
                  <p class="  text-center pe-4 pt-1 text-color5 fw-bold mt-4 ">{{__('ui.published')}}{{$announcement->created_at->format('d/m/Y')}}</p>
                </div>
              
            </div>
          </div>
          @endforeach
        
      
    </div>
  </div>
  @auth
  <div class="container">
    <div class="row ">
      <div class="col-12   d-flex justify-content-center">
        <div class=" pb-5 py-5 ">
          <div class="card-button bg-color1  py-3 px-3">
            <a class='text-color1 d-flex justify-content-center nounderline ' href="{{route('announcements.create')}}">{{__('ui.proponiAnnuncio')}}
            </a>
            <div class="">
              <a href="{{route('announcements.create')}}" class="fa-solid fa-paper-plane fa-2x font d-flex justify-content-center nounderline"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @endauth
  <x-card />
  <x-servizi />
  <!-- categoria -->
  {{-- si vede da tablet in su  --}}
  <div class="container  ">
    
    <div class="row d-none d-md-flex">
      
      
      <div class="col-12 d-flex justify-content-between pb-3  pt-5 ">
        <h2 class=" text-color3 fw-bold">{{__('ui.categoriaHome')}}</h2>
        <a href="{{route('categorie')}}"> <button class="btn btn-custom fw-bold text-white text-uppercase ">{{__('ui.vediTutto')}}</button></a >
        </div>
        <!-- card -->
        <div class="col-4   ">
          <a>
            <div class="bg-card2 bg-img d-flex justify-content-between mb-5">
              
              <h4 class="align-items-center d-flex text-white   ms-3">Arcade</h4>
              <img class="img-fluid  transform1 " src="/media/category-icon2.webp">
            </div>
          </a>
        </div>
        <!-- card2 -->
        <div class="col-4   ">
          <a  >
            
            <div class="bg-card2 bg-img4 d-flex justify-content-between">
              <h4 class="align-items-center d-flex text-white ms-1   ">FPS</h4>
              <img class="img-fluid  transform3 " src="./media/Overwatch-Character-PNG-Image-File.png">
            </div>
          </a>
        </div>
        <!-- card3 -->
        <div class="col-4   ">      
          <a >
            <div class="bg-card2 bg-img3 d-flex justify-content-between">
              <h4 class="align-items-center d-flex text-white nounderline  ms-3 pb-5 ">Indie</h4>
              <img class="img-fluid  transform2  " src="./media/the-devil-680032-removebg-preview (2).png">
            </div>
          </a>
        </div>
      </div>
      {{-- categoria --}}
      <div class="col-12 d-flex justify-content-between pb-5 pt-5 d-flex d-md-none">
        <h2 class=" text-color3 fw-bold">{{__('ui.categoria')}}</h2>
        <button class="btn btn-custom fw-bold text-white text-uppercase ">{{__('ui.vediTutto')}}</button>
      </div>
      <!-- card si vede da cellulare -->
      <div class="row d-flex d-md-none ">
        <div class="col-4 pt-5 ">
          <div class="card-900 d-flex justify-content-center   ">
            <a class="text-decoration text-center fw-bold text-uppercase "href="#">Arcade</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900 d-flex justify-content-center   ">
            <a class="text-decoration text-center fw-bold text-uppercase"href="#" >{{__('ui.picchiaduro')}}</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900  d-flex justify-content-center ">
            <a class="text-decoration text-center fw-bold text-uppercase " href="#">Indie</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900 d-flex justify-content-center  ">
            <a class="text-decoration text-center fw-bold text-uppercase "href="#">FPS</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900 d-flex justify-content-center   ">
            <a class="text-decoration text-center fw-bold text-uppercase"href="#" >{{__('ui.corse')}}</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900  d-flex justify-content-center ">
            <a class="text-decoration text-center fw-bold text-uppercase " href="#">Souls-Like</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900 d-flex justify-content-center  ">
            <a class="text-decoration text-center fw-bold text-uppercase "href="#">{{__('ui.azione')}}</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900 d-flex justify-content-center   ">
            <a class="text-decoration text-center fw-bold text-uppercase"href="#" >{{__('ui.carte')}}</a>
          </div>
        </div>
        <div class="col-4 pt-5">
          <div class="card-900  d-flex justify-content-center ">
            <a class="text-decoration text-center fw-bold text-uppercase " href="#">RPG</a>
          </div>
        </div>
        <div class="col-4 pt-5 pb-5">
          <div class="card-900 d-flex justify-content-center">
            <a class="text-decoration text-center fw-bold text-uppercase "href="#">Sandbox</a>
          </div>
        </div>
        <div class="col-4 pt-5 pb-5">
          <div class="card-900 d-flex justify-content-center">
            <a class="text-decoration text-center fw-bold text-uppercase"href="#" >Horror</a>
          </div>
        </div>
        <div class="col-4 pt-5 pb-5">
          <div class="card-900  d-flex justify-content-center ">
            <a class="text-decoration text-center fw-bold text-uppercase " href="#">Moba</a>
          </div>
        </div>
      </div>
    </div>
  </x-layout>
  