<x-layout>
    <x-slot name="header">Dettagli</x-slot>

    <div class="container-fluid  ">
        <div class="row   d-flex ">
            <div class="col-12 text-white  text-center fw-bold pt-3">
                <h2 class="">{{__('ui.announcement')}} {{$announcement->title}}</h2>
            </div>
            <div class="col-12 text-light bg-ff  text-center">             
            </div>
        </div>
    </div>

    @if($announcement)
  
  
    <div class="container pt-3 pb-3 bg-color">
      <div class="row">
        <div class="col-12 pb-5">
          <div id="dettaglioCarousel" class="carousel slide" data-bs-ride="carousel">
            @if ($announcement->images)
              <div class="carousel-inner ">
                  @foreach ($announcement->images as $image)
                    <div class="carousel-item dettaglioCarousel  @if($loop->first)active @endif">
                      <img src="{{Storage::url($image->path)}}" class=" img-fluid" alt="...">
                    </div>
                  @endforeach
              </div>
            @else 
                <div class="carousel-inner dettaglioCarousel  " >
                  <div class="carousel-item active">
                    <img src="htpps//picsum.photos/id/27/1200/400" class="img-fluid p-3 " alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="htpps//picsum.photos/id/27/1200/400" class="img-fluid p-3 " alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="htpps//picsum.photos/id/27/1200/400" class="img-fluid p-3 " alt="...">
                  </div>
                </div>
          @endif  
            
          
          <button class="carousel-control-prev" type="button" data-bs-target="#dettaglioCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#dettaglioCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
        <div class="text-center mt-5" > 
          <h5 class="text-light">{{__('ui.title')}} {{$announcement->title}}</h5>
          <p class="text-light">{{__('ui.descrizione')}} {{$announcement->body}}</p>
          <p class="text-light">{{__('ui.categoria')}} {{$announcement->category->name}}</p>
          <p class="text-light">{{__('ui.published')}} {{$announcement->created_at->format('d/m/Y')}}</p>
        </div>
         
          
          
      </div>
    </div>
  </div>
  @endif   

  <div class="bg-ff "></div>
  
</x-layout>

