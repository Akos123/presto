<x-layout>
    <x-slot name="header">{{__('ui.indexAnnouncementHeader')}}</x-slot>
    <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row">
              @forelse ($announcements as $announcement)
              <div class="col-12 col-md-4 my-4 pt-5">
                <div class=" cardR  "  >
                      <img class="img-fluid ridimensionamento " src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400 , 300) : 'https://picsum.photos/200'}}">
                      <div class="info">
                        <h4 class="font-1 pt-3 text-center font-1 text-uppercase text-color5 fw-bold ">{{$announcement->title}}</h5>
                          <p class="text-center font-1  text-color5 fw-bold display-6 ">{{$announcement->price}}€</p>
                          <div class=" d-flex justify-content-around   ">
                            <a href="{{route('announcements.show' ,compact('announcement'))}}" class="  ms-2 text-color4 fw-bold btn-custom5 py-1">{{__('ui.visualizza')}}</a>
                            <a href="" class=" btn-custom4 me-2 text-color4 fw-bold py-1"> {{__('ui.categoria')}}{{$announcement->category->name}}</a>
                          </div>
                          <p class="  text-center pe-4 pt-3 text-color5 fw-bold ">{{__('ui.published')}}{{$announcement->created_at->format('d/m/Y')}}</p>
                      </div>
                  
                </div>
              </div>
              @empty

              <div class="col-12">
                <div class="alert alert-warning py-3 shadow">
                  <p class="lead">{{__('ui.alertNoAnn')}}</p>
                </div>  
              </div>     
        
              @endforelse
              {{$announcements->links()}}
            </div>
          </div>
        </div>
      </div>

      @auth
      <div class="container">
        <div class="row ">
          <div class="col-12 d-flex justify-content-center">
            <div class=" pb-5 py-5 ">
              <div class="card-button bg-color1  py-3 px-3">
                <a class='text-color1 d-flex justify-content-center nounderline ' href="{{route('announcements.create')}}">{{__('ui.proponiAnnuncio')}}
                </a>
                <div class="">
                  <a href="{{route('announcements.create')}}" class="fa-solid fa-paper-plane fa-2x font d-flex justify-content-center nounderline"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @else
      @endauth
</x-layout>