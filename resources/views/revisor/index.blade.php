<x-layout>
  <x-slot name='header'>pagina revisione</x-slot>
  
  <div>
    <h2 class="text-light text-center p-2">{{$announcement_to_check ? 'Ecco l\'annuncio da revisionare': 'Non ci sono annunci da revisionare'}}</h2>
  </div>
  
  @if($announcement_to_check)
  
  
    <div class="container pt-3 pb-3 bg-color">
      <div class="row">
        <div class="col-12 pb-5">
          <div id="revisorCarousel" class="carousel slide" data-bs-ride="carousel">
            @if ($announcement_to_check->images)
              <div class="carousel-inner ">
                  @foreach ($announcement_to_check->images as $image)
                    <div class="carousel-item customCarousel  @if($loop->first)active @endif">
                      <img src="{{Storage::url($image->path)}}" class=" img-fluid" alt="...">
                    </div>
                  @endforeach
              </div>
            @else 
                <div class="carousel-inner revisorCarousel  " >
                  <div class="carousel-item active">
                    <img src="htpps//picsum.photos/id/27/1200/400" class="img-fluid p-3 " alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="htpps//picsum.photos/id/27/1200/400" class="img-fluid p-3 " alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="htpps//picsum.photos/id/27/1200/400" class="img-fluid p-3 " alt="...">
                  </div>
                </div>
          @endif  
            
          
          <button class="carousel-control-prev" type="button" data-bs-target="#revisorCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#revisorCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
        <div class="text-center mt-5" > 
          <h5 class="text-light">{{__('ui.title')}} {{$announcement_to_check->title}}</h5>
          <p class="text-light">{{__('ui.descrizione')}} {{$announcement_to_check->body}}</p>
          <p class="text-light">{{__('ui.categoria')}} {{$announcement_to_check->category->name}}</p>
          <p class="text-light">{{__('ui.published')}} {{$announcement_to_check->created_at->format('d/m/Y')}}</p>
        </div>
         
          <div class="row">
              <div class="col-12 col-md-6 py-5 ">
                <form action="{{route('revisor.accept_announcement', ['announcement'=>$announcement_to_check])}}" method="POST">
                  @csrf
                  @method("PATCH")
                  <button type="submit" class="btn-custom4 text-light ">{{__('ui.accetta')}}</button>
                  
                </form>
              </div>

              <div class="col-12 col-md-6 text-end py-5">  
                <form action="{{route('revisor.reject_announcement', ['announcement'=>$announcement_to_check])}}" method="POST">
                  @csrf
                  @method("PATCH")
                  <button type="submit" class="btn-custom4 text-light">{{__('ui.rifiuta')}}</button>
                  
                </form>
              </div>
          </div>
          
      </div>
    </div>
    {{-- card ia di google da modificare --}}
   
    @if ($announcement_to_check->images)
    <div class="row">
        @foreach ($announcement_to_check->images as $image)

        <div class="col-12 col-md-6 text-light">
          <h5 class="tc-accent mt-3">Tags</h5>
          <div class="p-2">
            @if ($image->labels)
                @foreach ($image->labels as $label)
                  <p class="d-inline">{{$label}},</p>
                @endforeach
            @endif      
          </div>

        </div>
        <div class="col-12 col-md-6 text-light">
          <div class="card-body text-white">
            <h5 class="tc-accent">Revisione immagini</h5>
              <p>Adulti: <i class="{{$image->adult}}"></i></p>
              <p>Satira: <span class="{{$image->spoof}}"></span></p>
              <p>Medicina: <span class="{{$image->medical}}"></span></p>
              <p>Violenza: <span class="{{$image->violence}}"></span></p>
              <p>Contenuto ammiccante: <span class="{{$image->racy}}"></span></p>
            
          </div>
        </div>
        @endforeach
    </div>
    @endif 
   
      
    </div>

  @endif   
  
      
  
  
  
</x-layout>
